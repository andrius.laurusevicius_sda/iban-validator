## Lithuanian IBAN validator

The app checks Lithuanian IBAN from console input or imports IBAN list from the file. 

#### How to run the program:

- git clone https://gitlab.com/andrius.laurusevicius_sda/iban-validator.git the project.
- Run  main class App.java

#### App screenshots:

_User input validation:_

![alt text](https://i.ibb.co/zSVWzwQ/manualibanvalidation.png)

_IBAN accounts import and validation:_

![alt text](https://i.ibb.co/5hpsLdX/importedaccountscheck.png)