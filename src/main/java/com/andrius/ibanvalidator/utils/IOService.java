package com.andrius.ibanvalidator.utils;

import java.util.Scanner;

/**
 * Code created by Andrius on 2020-09-21
 */
public class IOService {

    public static void printWelcomeMessage() {
        String line = "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=";
        System.out.printf("%s%nIBAN validator for Lithuanian bank accounts.%n%s%n", line, line);
    }

    public static void printQuestion() {
        System.out.println("\nTo input IBAN code manually enter - 'm', to import enter - 'i', to exit - enter 0:");
    }

    public static void printMessage(String message) {
        System.out.println(message);
    }

    public static String getUserInput() {
        return new Scanner(System.in).nextLine();
    }
}
