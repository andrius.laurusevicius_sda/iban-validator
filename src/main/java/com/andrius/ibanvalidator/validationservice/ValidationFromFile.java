package com.andrius.ibanvalidator.validationservice;

import com.andrius.ibanvalidator.dbservice.ReaderFromFile;

/**
 * Code created by Andrius on 2020-07-15
 */
public class ValidationFromFile {

    public void printValidationResults() {
        ReaderFromFile.getListOfIbanAccounts().stream()
                .map(s -> s + ";" + IBANValidator.getValidationAnswer(s))
                .forEach(System.out::println);
    }
}