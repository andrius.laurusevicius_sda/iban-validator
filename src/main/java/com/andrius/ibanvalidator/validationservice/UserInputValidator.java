package com.andrius.ibanvalidator.validationservice;

import com.andrius.ibanvalidator.utils.IOService;

/**
 * Code created by Andrius on 2020-07-15
 */
public class UserInputValidator {

    public void printValidationResults() {
        String iban = IOService.getUserInput();
        boolean result = IBANValidator.getValidationAnswer(iban);
        if (result) {
            System.out.println("Your IBAN code " + iban + " is correct");
        } else {
            System.out.println("Wrong IBAN format.");
        }
    }
}
