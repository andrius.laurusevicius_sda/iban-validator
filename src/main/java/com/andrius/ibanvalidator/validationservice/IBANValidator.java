package com.andrius.ibanvalidator.validationservice;

import java.math.BigInteger;

/**
 * Code created by Andrius on 2020-07-15
 */
public class IBANValidator {

    public static final int IBAN_LENGTH = 20;
    private static final int MOD = 97;
    private static final int REMAINDER = 1;

    private static BigInteger getInputNumericValue(String ibanString) {
        String ibanNumber = ibanString.substring(4, IBAN_LENGTH) +
                Character.getNumericValue(ibanString.charAt(0)) +
                Character.getNumericValue(ibanString.charAt(1)) +
                ibanString.substring(2, 4);
        return new BigInteger(ibanNumber);
    }

    public static boolean getValidationAnswer(String ibanString) {
        if (ibanString.length() == IBANValidator.IBAN_LENGTH) {
            return getInputNumericValue(ibanString).mod(BigInteger.valueOf(MOD)).equals(BigInteger.valueOf(REMAINDER));
        }
        return false;
    }
}
