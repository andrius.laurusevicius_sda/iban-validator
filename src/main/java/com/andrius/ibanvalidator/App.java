package com.andrius.ibanvalidator;

import com.andrius.ibanvalidator.controller.InputSource;

/**
 * Code created by Andrius on 2020-07-15
 */
public class App {
    public static void main(String[] args) {

        new InputSource().runApp();
    }
}
