package com.andrius.ibanvalidator.dbservice;

import com.andrius.ibanvalidator.utils.IOService;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Code created by Andrius on 2020-07-15
 */
public class ReaderFromFile {

    public static final String PATH = getPath();

    public static List<String> getListOfIbanAccounts() {
        List<String> list = new ArrayList<>();

        try (Scanner input = new Scanner(new File(PATH + ".txt"))) {
            while (input.hasNextLine()) {
                list.add(input.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.err.println("Wrong path to file. Try demo.");
        }
        return list;
    }

    private static String getPath() {
        IOService.printMessage("Enter the path to folder(demo path - src/main/resources/):");
        String path = IOService.getUserInput();
        IOService.printMessage("Enter the file name(demo name - accounts):");
        String name = IOService.getUserInput();
        return path + name;
    }
}
