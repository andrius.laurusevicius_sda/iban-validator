package com.andrius.ibanvalidator.dbservice;

import com.andrius.ibanvalidator.validationservice.IBANValidator;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Code created by Andrius on 2020-07-28
 */
public class WriterToFile {

    public void writeValidationResultsToFile() {
        try (PrintWriter writer = new PrintWriter(new FileWriter(ReaderFromFile.PATH + "out.txt"))) {
            for (String account : ReaderFromFile.getListOfIbanAccounts()) {
                writer.println(account + ";" + IBANValidator.getValidationAnswer(account));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
