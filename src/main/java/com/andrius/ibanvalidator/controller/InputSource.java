package com.andrius.ibanvalidator.controller;

import com.andrius.ibanvalidator.dbservice.WriterToFile;
import com.andrius.ibanvalidator.utils.IOService;
import com.andrius.ibanvalidator.validationservice.UserInputValidator;
import com.andrius.ibanvalidator.validationservice.ValidationFromFile;

/**
 * Code created by Andrius on 2020-07-15
 */
public class InputSource {

    public void runApp() {
        IOService.printWelcomeMessage();
        while (true) {
            IOService.printQuestion();
            validateIBAN();
        }
    }

    private void validateIBAN() {
        String userInput = IOService.getUserInput().toLowerCase();
        if (userInput.equals("m")) {
            validateManual();
        } else if (userInput.equals("i")) {
            validateFromFile();
        } else if (userInput.equals("0")) {
            System.exit(0);
        } else {
            IOService.printMessage("Wrong selection, try again!");
            validateIBAN();
        }
    }

    private void validateManual() {
        IOService.printMessage("Enter IBAN number to validate:");
        new UserInputValidator().printValidationResults();
    }

    private void validateFromFile() {
        new ValidationFromFile().printValidationResults();
        new WriterToFile().writeValidationResultsToFile();
    }
}
